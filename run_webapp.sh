#! /usr/bin/env bash

set -e

if [ -f .env ]; then
  source .env
fi

RUN_COMMAND="talisker.gunicorn newsletter.newsletter:app --bind $1 --worker-class sync --workers 4 --name talisker-`hostname`"

if [ "${FLASK_DEBUG}" = true ] || [ "${FLASK_DEBUG}" = 1 ]; then
    RUN_COMMAND="${RUN_COMMAND} --reload --log-level debug --timeout 9999"
fi

${RUN_COMMAND}
