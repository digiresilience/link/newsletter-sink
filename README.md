# newsletter-sink

A service that accepts an email via a POST and sends it to a signal contact

## Development

1. Create your python3 virtualenv, then `pip install -r requirements.txt`
2. Setup your vars, see `env.sample`
3. Run the web server `./run_webapp.sh`


## Production

The `deployment` folder has a systemd service file and nginx conf.


# License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/link/sigarillo/blob/master/LICENSE.md)

Sigarillo is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital Resilience](https://digiresilience.org) and [Guardian Project](https://guardianproject.info).
