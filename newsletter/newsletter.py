import os
import requests
from flask import request
from talisker.flask import TaliskerApp
from logging import getLogger
from werkzeug.contrib.fixers import ProxyFix
from werkzeug.debug import DebuggedApplication
from flask_cors import CORS

from newsletter.util import validate_cfg, get_cfg, is_devel_mode

validate_cfg()
app = TaliskerApp(__name__)
cors = CORS(app, resources={r"/subscribe": {"origins": "*"}})
app.wsgi_app = ProxyFix(app.wsgi_app)
logger = getLogger(__name__)
if app.debug:
    app.wsgi_app = DebuggedApplication(app.wsgi_app)


def signal_send(msg):
    domain = get_cfg("SIGARILLO_DOMAIN")
    api_key = get_cfg("SIGARILLO_API_KEY")
    recipient = get_cfg("SIGARILLO_RECIPIENT")
    payload = {"recipient": recipient, "message": msg}
    url = "https://{}/bot/{}/send".format(domain, api_key)
    logger.debug(payload)
    r = requests.post(url, data=payload)
    return r.status_code


@app.route("/subscribe", methods=["POST"])
def subscribe():
    """
    Accepts a form POST with an email attribute
    """
    email = request.form["email"]
    msg = "Newsletter subscribe request: {}".format(email)
    logger.debug(msg)
    r = signal_send(msg)
    logger.debug("response from signal %s", r)
    if r == 200:
        return "", 201
    else:
        return "error", r


def main():
    debug = os.getenv("DEBUG", "true").lower() == "true" or is_devel_mode()
    app.run(debug=debug, host="127.0.0.1", port=5000)


if __name__ == "__main__":
    main()
