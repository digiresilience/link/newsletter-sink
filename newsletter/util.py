import os


def get_cfg(key, default=None):
    """Returns a value from the environment, first trying the passed key
    then trying the _FILE variant, reading the value from the file if it exists
    """
    try:
        return os.environ[key]
    except KeyError:
        try:
            with open(os.environ[key + "_FILE"], "r") as f:
                return f.read().strip()
        except (KeyError, OSError):
            return default


class ConfigurationError(Exception):
    pass


def validate_cfg():
    required_keys = ["BASE_URL", "SIGARILLO_DOMAIN", "SIGARILLO_API_KEY", "SIGARILLO_RECIPIENT"]
    missing = []
    for k in required_keys:
        v = get_cfg(k)
        if v is None:
            missing.append(k)

    if len(missing) > 0:
        raise ConfigurationError(
            "Required config values not found: {}".format(", ".join(missing))
        )
    base_url = get_cfg("BASE_URL")
    if not base_url.startswith("http"):
        raise ConfigurationError(
            'BASE_URL value "{}" invalid. Must be a valid url like "https://example.com"'.format(
                base_url
            )
        )


def is_devel_mode():
    v = get_cfg("DEVEL", False)
    return v is True or v == "true" or v == "1"
