"""Installer for newsletter-sink
"""

import os
from setuptools import setup, find_packages


setup(
    name="newsletter-sink",
    description="A micro service that accepts a form submission and sends it to a signal group",
    long_description=open("README.md").read(),
    version="0.0.1",
    author="Abel Luck",
    author_email="abel@guardianproject.info",
    url="https://gitlab.com/digiresilience/link/newsletter-sink",
    packages=find_packages(exclude=["ez_setup"]),
    install_requires=open(
        os.path.join(os.path.dirname(__file__), "requirements.txt")
    ).readlines(),
    entry_points={"console_scripts": ["newsletter-sink = newsletter.newsletter:main"]},
    license="AGPL3",
)
